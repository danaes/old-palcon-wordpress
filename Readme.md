# Palcon Dev Environment

This is a docker environment to developm Palcon stuff

## Installation

Clone the repository

## Usage

In order to run the environment please run:

```sh
docker-compose up
```

In order to stop the environment please run:

```sh
docker-compose down
```

You can access to the envrionment via http://localhost

## Start development

To develop new or existing plugins, create a `plugins` directory if is not already created.

then clone plugins repositories inside plugins directory you've created.

To develop new or existing themes, create a `themes` directory if is not already created.


## Import database

First create a dumps folder in the root if is not already created, after that put the .sql file inside.
Then run the next commands:

First run `docker-compose build` to build Dockerfile changes.

```sh
docker-compose up

docker-compose run web bash
```

This last command will put you inside the web container, run `cd ../dumps`

Once you get to that directory you can run the following commands:

```sh
mysql -hmysql -upalcon -ppalcon
```

Once you are inside mysql cli run:

```sh
drop database palcon;
create database palcon;
use palcon;

source FILENAME.sql
```

Change FILENAME with the actual name of you sal file.
Wait to the import to finish;
